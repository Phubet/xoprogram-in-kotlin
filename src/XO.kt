import java.lang.NumberFormatException
val table = arrayOf(charArrayOf('-', '-', '-'),
        charArrayOf('-', '-', '-'),
        charArrayOf('-', '-', '-'))
var currentPlayer = 'X'
var turnCount = 0

fun main() {
    sayWelcome()
    while (true){
        printTable()
        showTurn()
        input()
        if (checkWinner() || turnCount == 8){
            break
        }
        switchPlayer()
    }
    printTable()
    showWinner()

}

private fun checkWinner(): Boolean {
    if (checkRow(0) || checkRow(1) || checkRow(2)) {
        return true
    }else if (checkCol(0) || checkCol(1) || checkCol(2)) {
        return true
    }else if (checkLeftDiagonal() || checkRightDiagonal()) {
        return true
    }
    return false
}

private fun showWinner() {
    if (turnCount == 8) {
        println("Draw!!!")
    }else {
        println(currentPlayer + " is Winner!!!")
    }
}

private fun checkRow(x: Int): Boolean {
    for (row in table) {
       if (table[x][0].equals(currentPlayer) && table[x][1].equals(currentPlayer) &&table[x][2].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkCol(x: Int): Boolean {
    for (row in table) {
        if (table[0][x].equals(currentPlayer) && table[1][x].equals(currentPlayer) &&table[2][x].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkLeftDiagonal(): Boolean {
    for (row in table) {
       if (table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer))
            return true
    }
    return false
}

private fun checkRightDiagonal(): Boolean {
    for (row in table) {
        if (table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer))
            return true
    }
    return false
}

private fun sayWelcome() {
    println("Welcome to game XO in Kotlin!")
}

private fun showTurn() {
    println(currentPlayer + " Turn!")
}

private fun switchPlayer() {
    if (currentPlayer == 'X') {
        currentPlayer = 'O'
    } else {
        currentPlayer = 'X'
    }
    turnCount += 1
}

private fun input() {
    while (true) {
        try {
            print("Please input (row,col): ")
            val input = readLine()
            val numStr = input?.split(" ")
            if (numStr?.size == 2) {
                val row = numStr?.get(0)?.toInt()-1
                val col = numStr?.get(1)?.toInt()-1
                if (table[row][col] == '-') {
                    table[row][col] = currentPlayer
                }else {
                    println("Is not empty!")
                    continue
                }
                break
            } else {
                println("Please input again! (ex: 1 1)")
            }
        } catch (e: NumberFormatException) {
            println("Please input again! (ex: 1 1)")
        } catch (e: ArrayIndexOutOfBoundsException) {
            println("Please input again! (ex: 1 1)")
        }
    }
}

private fun printTable() {
    for (row in table) {
        for (col in row) {
            print(col)
        }
        println()
    }
}